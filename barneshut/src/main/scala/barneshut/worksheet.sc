import barneshut.Body
import barneshut.Leaf

val body1 = new Body(12, 7, 8, 0, 0)

val leaf = Leaf(10, 10, 10, Seq(body1))

val body2 = new Body(2, 12, 13, 0, 0)

leaf.insert(body2)
